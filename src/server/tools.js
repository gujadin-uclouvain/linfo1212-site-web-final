//// FUNCTIONS SECTION - START
exports.Alert =
class Alert {
	constructor() {
		this._travel_subscribe = "Inscription au voyage acceptée"; this._travel_noplace = "Il n'y a plus de place dans pour ce voyage"; this._travel_remove = "Voyage supprimé avec succès"; this._travel_success = "Voyage ajouté avec succès";
		this._connect_wrong = "Nom d'utilisateur et/ou mot de passe incorrect"; this._connect_success = "Connexion réussie"; this._connect_before = "Connectez-vous avant d'ajouter un voyage";
		this._subscribe_exist = "Le nom d'utilisteur rentré existe déjà"; this._subscribe_created = "Nouveau compte créé avec succès";
		this._name_unchanged = "Prénom et nom inchangés"; this._name_fname = "Prénom modifié avec succès"; this._name_lname = "Nom modifié avec succès"; this._name_flname = "Prénom et nom modifiés avec succès";
		this._pswd_wrong = "Mot de passe incorrect"; this._pswd_modified = "Mot de passe modifié avec succès";
		this._email_unchanged = "Email inchangée"; this._email_modified = "Email modifié avec succès";
		this._picture_reset = "Image réinitialisée avec succès"; this._picture_set = "Image modifiée avec succès";
		this._missing = "Veuillez remplir tous les champs obligatoires";
		this._disconnect = "Déconnexion réussie";
		this._error = "Une erreur a été rencontrée...";
	}

	send(req) { let alert = req.session.alert; req.session.alert = undefined; return alert; }
	static send(req) { let alert = req.session.alert; req.session.alert = undefined; return alert; }

	parse_html(msg) { return `<script>M.toast({html: "${msg}"});</script>`; }
	static parse_html(msg) { return `<script>M.toast({html: "${msg}"});</script>`; }

	get travel_subscribe() { return this.parse_html(this._travel_subscribe); }
	get travel_noplace() { return this.parse_html(this._travel_noplace); }
	get travel_remove() { return this.parse_html(this._travel_remove); }
	get travel_success() { return this.parse_html(this._travel_success); }

	get connect_wrong() { return this.parse_html(this._connect_wrong); }
	get connect_success() { return this.parse_html(this._connect_success); }
	get connect_before() { return this.parse_html(this._connect_before); }

	get subscribe_exist() { return this.parse_html(this._subscribe_exist); }
	get subscribe_created() { return this.parse_html(this._subscribe_created); }

	get name_unchanged() { return this.parse_html(this._name_unchanged); }
	get name_fname() { return this.parse_html(this._name_fname); }
	get name_lname() { return this.parse_html(this._name_lname); }
	get name_flname() { return this.parse_html(this._name_flname); }

	get pswd_wrong() { return this.parse_html(this._pswd_wrong); }
	get pswd_modified() { return this.parse_html(this._pswd_modified); }

	get email_unchanged() { return this.parse_html(this._email_unchanged); }
	get email_modified() { return this.parse_html(this._email_modified); }

	get picture_reset() { return this.parse_html(this._picture_reset); }
	get picture_set() { return this.parse_html(this._picture_set); }

	get missing() { return this.parse_html(this._missing); }
	get disconnect() { return this.parse_html(this._disconnect); }
	get error() { return this.parse_html(this._error); }
};

exports.Time =
class Time {
	constructor() {
		let d = new Date();
		this.time =  {
			milsec: Time.milsec_parsing(d.getMilliseconds()),
			year: d.getFullYear(),
			full_month: d.toLocaleString('default', {month: 'long'}),
			month: Time.time_parsing(d.getMonth() + 1),
			day: Time.time_parsing(d.getDate()),
			hour: Time.time_parsing(d.getHours()),
			min: Time.time_parsing(d.getMinutes()),
			sec: Time.time_parsing(d.getSeconds()),
		};
	}

	static time_parsing(elem) {
		let result = elem.toString();
		if (result.length === 1) { result = "0" + result; }
		return result;
	}

	static milsec_parsing(elem) {
		let result = elem.toString();
		if (result.length === 1) { result = "00" + result; }
		else if (result.length === 2) { result = "0" + result; }
		return result;
	}

	static full_month_to_number(full_month) {
		const month_fr = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
		for (let index = 0; index < month_fr.length; index++) {
			if (month_fr[index] === full_month) {
				return Time.time_parsing(index+1).toString();
			}
		}
		return null;
	}

	static parse_date_with_full_month_to_id(full_date) {
		let travel_time_array = full_date.split(" ");
		return travel_time_array[2] + Time.full_month_to_number(travel_time_array[1]) + travel_time_array[0];
	}

	static parse_hour_to_id(hour) {
		let travel_hour_array = hour.split(":");
		return travel_hour_array[0] + travel_hour_array[1];
	}

	static parse_full_time_to_id(d) {
		let to_parse = d.split(' | ');
		let date = to_parse[0].split('/');
		let time = to_parse[1].split(':');
		return date[2] + date[1] + date[0] + time[0] + time[1] + time[2] + time[3];
	}

	setup_date_main() {
		return this.time.day + " " + this.time.full_month + " " + this.time.year;
	}

	setup_date_travels() {
		return this.time.day + "/" + this.time.month + "/" + this.time.year + " | " + this.time.hour + ":" + this.time.min + ":" + this.time.sec + ":" + this.time.milsec;
	}

	id_maker() {
		return "" + this.time.year + this.time.month + this.time.day + this.time.hour + this.time.min + this.time.sec + this.time.milsec;
	}

	is_outdated(full_date) {
		return Time.parse_date_with_full_month_to_id(full_date) < ("" + this.time.year + this.time.month + this.time.day);
	}
};

exports.no_found_generator_html =
function (type) {
	let msg;
	if (type === "offer") { msg = "Il semble n'y avoir aucun trajet de prévu en ce moment.<br>Soyez le premier à en ajouter un..."; }
	else if (type === "demand") { msg = "Il semble n'y avoir aucune demande de covoiturage.<br>Soyez le premier à en ajouter une..."; }
	else if (type === "subscribe") { msg = "Il semble que vous n'ayez pas encore souscrit à une offre.<br>N'attendez plus..."; }
	return `<br><div class="container row center"><div class="col s12"><div class="center card-panel green darken-2 z-depth-2"><span class="white-text">${msg}</span></div></div></div>`;
};

exports.set_current_user =
function (req, is_new, data_user = undefined) {
	if (is_new) {
		req.session.current_username = req.body.create_username;
		req.session.current_first_name = req.body.create_first_name;
		req.session.current_last_name = req.body.create_last_name;
		req.session.current_email = req.body.create_email;
		req.session.current_picture = "../images/user_neutral.png";
	} else {
		if (data_user !== undefined) {
			req.session.current_username = req.body.user;
			req.session.current_first_name = data_user.first_name;
			req.session.current_last_name = data_user.last_name;
			req.session.current_email = data_user.email;
			req.session.current_picture = data_user.picture;
		}
	}
};

exports.reset_current_user =
function (req) {
	req.session.current_username = undefined;
	req.session.current_first_name = undefined;
	req.session.current_last_name = undefined;
	req.session.current_email = undefined;
	req.session.current_picture = undefined;
};

exports.changing_name =
function (req, data_user, alert) {
	if (data_user === undefined) {
		req.session.alert = alert.name_unchanged;
		return null;
	}
	let input_fname = req.body.modify_first_name_new.trim();
	let input_lname = req.body.modify_last_name_new.trim();
	let sender;

	if ((input_fname !== data_user.first_name && input_fname !== "") && (input_lname !== data_user.last_name && input_lname !== "")) {
		sender = {"first_name" : input_fname, "last_name" : input_lname};
		req.session.alert = alert.name_flname; req.session.current_first_name = input_fname; req.session.current_last_name = input_lname;
	} else if ((input_fname !== data_user.first_name && input_fname !== "") && (input_lname === data_user.last_name || input_lname === "")) {
		sender = {"first_name" : input_fname};
		req.session.alert = alert.name_fname; req.session.current_first_name = input_fname;
	} else if ((input_fname === data_user.first_name || input_fname === "") && (input_lname !== data_user.last_name && input_lname !== "")) {
		sender = {"last_name" : input_lname};
		req.session.alert = alert.name_lname; req.session.current_last_name = input_lname;
	} else {
		req.session.alert = alert.name_unchanged;
		sender = null;
	}
	return sender;
};

exports.parse_filter_and_order =
function (req) {
	if (req.session.filter === "id_sort_start_date") {
		return {id_sort_start_date: req.session.order};
	} else if (req.session.filter === "start_where") {
		return {start_where: req.session.order};
	} else if (req.session.filter === "arrive_where") {
		return {arrive_where: req.session.order};
	} else if (req.session.filter === "price") {
		return {price: req.session.order};
	} else if (req.session.filter === "nbr_passengers") {
		return {nbr_passengers: req.session.order};
	} else if (req.session.filter === "travel_user") {
		return {travel_user: req.session.order};
	} else if (req.session.filter === "_id") {
		return {_id: req.session.order};
	}
	return {id_sort_start_date: 1};
};

exports.which_filter_order_selected =
function (req, elem) {
	let order_or_filter = (elem === 1 || elem === -1) ? req.session.order : req.session.filter;
	if (order_or_filter === elem) {
		return 'teal white-text';
	}
	return undefined;
};

exports.is_full_color =
function (data) {
	if (data.travel_type === "O") {
		return (data.nbr_passengers === data.nbr_actual_passengers) ? '#ef6101' : '#000000';
	} else {
		return '#000000';
	}
};
//// FUNCTIONS SECTION - END

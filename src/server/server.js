//// IMPORT NODEJS MODULES - START
const express = require('express');
const consolidate = require('consolidate');
const MongoClient = require('mongodb').MongoClient;
const session = require('express-session');
const bodyParser = require('body-parser');
const https = require('https');
const fs = require('fs');
//// IMPORT MODULES - END
//
//
//// IMPORT MY MODULES - START
const Alert = require('./tools').Alert;
const Time = require('./tools').Time;
const no_found_generator_html = require('./tools').no_found_generator_html;
const set_current_user = require('./tools').set_current_user;
const reset_current_user = require('./tools').reset_current_user;
const changing_name = require('./tools').changing_name;
const parse_filter_and_order = require('./tools').parse_filter_and_order;
const which_filter_order_selected = require('./tools').which_filter_order_selected;
const is_full_color = require('./tools').is_full_color;
//// IMPORT MY MODULES - END
//
//
//// SERVER ESTABLISHMENT - START
const app = express();
app.engine('html', consolidate.hogan);
app.set('view engine', 'html');
app.set('views','../webapp/views');
app.use(express.static('../webapp'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
	secret: "c@RP0oL!nG2O19",
	resave: false,
	saveUninitialized: true,
	cookie: {
		path: '/',
		httpOnly: true,
	}
}));
//// SERVER ESTABLISHMENT - END
//
//
/// DATABASE ESTABLISHMENT - START
const url = 'mongodb://localhost:27017';
const db_name = "carpool";
const options = { useNewUrlParser: true, useUnifiedTopology: true };
MongoClient.connect(url, options, (err, client) => {
	if (err) throw err;
	db = client.db(db_name);
	console.log(`Connected MongoDB: ${url}`);
	console.log(`Database Name: ${db_name}`);
	console.log('-------------------------');
});
/// DATABASE ESTABLISHMENT - END
//
//
/// VARIABLES ESTABLISHMENT - START
const alert = new Alert();
/// VARIABLES ESTABLISHMENT - END
//
//
//// GET/POST SECTION - START
// Main Page
app.get('/main',function(req, res) {
	res.redirect('/');
});

app.get('/',function(req, res) {
	let current_time = new Time();
	let data_offers = [];
	let data_demands = [];
	if (req.session.filter === undefined) { req.session.order = 1; req.session.filter = "id_sort_start_date"; }
	let cursor = db.collection('travels').find({}).sort( parse_filter_and_order(req) );
	cursor.forEach(function (doc) {
		if (!current_time.is_outdated(doc.start_date)) {
			doc.outdated_color = is_full_color(doc);
			doc.travel_type === "O" ? data_offers.push(doc) : data_demands.push(doc);
		}
	}, function () {
		res.render('main.html', {
			alert: Alert.send(req),
			date: current_time.setup_date_main(),
			current_first_name: req.session.current_first_name,
			current_last_name: req.session.current_last_name,
			current_email: req.session.current_email,
			current_picture: req.session.current_picture,
			is_connected: req.session.current_username,
			is_offers: data_offers.length !== 0,
			offers_list: data_offers,
			is_demands: data_demands.length !== 0,
			demands_list: data_demands,
			not_found_o: no_found_generator_html("offer"),
			not_found_d: no_found_generator_html("demand"),
			order_increase: which_filter_order_selected(req, 1), order_decrease: which_filter_order_selected(req, -1),
			filter_start_when: which_filter_order_selected(req, "id_sort_start_date"), filter_start_where: which_filter_order_selected(req, "start_where"), filter_arrive_where: which_filter_order_selected(req, "arrive_where"), filter_price: which_filter_order_selected(req, "price"), filter_nbr_passengers: which_filter_order_selected(req, "nbr_passengers"), filter_user: which_filter_order_selected(req, "travel_user"), filter_post: which_filter_order_selected(req, "_id"),
		});
	});
});

app.post('/main/filter', function(req, res) {
	if (req.body.filter_btn === "increase") {req.session.order = 1;}
	else if (req.body.filter_btn === "decrease") {req.session.order = -1;}
	else if (req.body.filter_btn === "start_when") {req.session.filter = "id_sort_start_date";}
	else if (req.body.filter_btn === "start_where") {req.session.filter = "start_where";}
	else if (req.body.filter_btn === "arrive_where") {req.session.filter = "arrive_where";}
	else if (req.body.filter_btn === "price") {req.session.filter = "price";}
	else if (req.body.filter_btn === "nbr_passengers") {req.session.filter = "nbr_passengers";}
	else if (req.body.filter_btn === "user") {req.session.filter = "travel_user";}
	else if (req.body.filter_btn === "post") {req.session.filter = "_id";}
	res.redirect('/main');
});

// Details Travel
app.post('/details', function(req, res) {
	let id_parsed = Time.parse_full_time_to_id(req.body.details_sender);
	db.collection('travels').findOne( {"_id" : id_parsed}, (err, travel) => {
		if (err) throw err;
		if (travel === null) {
			req.session.alert = alert.error;
			return res.redirect('/main');
		}
		let calc_nbr_passengers = parseInt(travel.nbr_passengers) - parseInt(travel.nbr_actual_passengers);
		db.collection('users').findOne({"username" : travel.travel_user}, (err, user) => {
			if (err) throw err;
			if (travel === null) {
				req.session.alert = alert.error;
				return res.redirect('/main');
			}
			res.render('details.html', {
				alert: Alert.send(req),
				date: new Time().setup_date_main(),
				current_first_name: req.session.current_first_name,
				current_last_name: req.session.current_last_name,
				current_email: req.session.current_email,
				current_picture: req.session.current_picture,
				is_connected: req.session.current_username,
				is_same_user: travel.travel_user === req.session.current_username,
				email_creator: user.email,
				travel_details: travel,
				subscribers: travel.travel_type === "O" ? travel.passengers.subscribers : null,
				calc_nbr_passengers: travel.travel_type === "O" ? calc_nbr_passengers : null,
				is_full: calc_nbr_passengers === 0,
				offer_or_demand: travel.travel_type === "O" ? "offre" : "demande",
				is_offer: travel.travel_type === "O",
			});
		});
	});
});

app.post('/details/subscribe', function(req, res) {
	db.collection('users').findOne( {"username" : req.session.current_username} , (err, data_user) => {
	    if (err) throw err;
	    if (data_user === null) {
			console.log(">> Modification Error");
			req.session.alert = alert.error;
			res.redirect('/main');
		}
		if (data_user.pswd === req.body.pswd_new_passengers) {
			let id_parsed = Time.parse_full_time_to_id(req.body.details_sender);
			db.collection('travels').findOne( {"_id" : id_parsed} , (err, data) => {
				if (err) throw err;
				if (data === null) {
					req.session.alert = alert.error;
					return res.redirect('/main');
				}
				let max_passengers = data.nbr_passengers;
				let new_passenger_place = parseInt(req.body.nbr_new_passengers) + data.nbr_actual_passengers;
				if (new_passenger_place <= max_passengers) {
					let new_passengers_names = data.passengers;
					let already_sub = new_passengers_names.subscribers.lastIndexOf(req.session.current_username);
					if (already_sub === -1) { // if the person doesn t subscribe yet
						new_passengers_names.subscribers.push(req.session.current_username);
						new_passengers_names.places.push(parseInt(req.body.nbr_new_passengers));  // add number of place for one person
					} else {
						new_passengers_names.places[already_sub] += parseInt(req.body.nbr_new_passengers)  // add number of place for one person
					}

					db.collection('travels').updateOne( {"_id": id_parsed}, {$set: {"nbr_actual_passengers": new_passenger_place, "passengers": new_passengers_names}});
					req.session.alert = alert.travel_subscribe;
					res.redirect('/main')
				} else {
					req.session.alert = alert.travel_noplace;
					return res.redirect('/main');
				}
			});
		} else {
			console.log(">> Details: Wrong Password");
			req.session.alert = alert.pswd_wrong;
			res.redirect('/main');
		}
	});
});

app.post('/details/delete', function(req, res) {
	db.collection('users').findOne( {"username" : req.session.current_username} , (err, data_user) => {
	    if (err) throw err;
	    if (data_user === null) {
			console.log(">> Modification Error");
			req.session.alert = alert.error;
			res.redirect('/main');
		}
		if (data_user.pswd === req.body.pswd_delete) {
			let id_parsed = Time.parse_full_time_to_id(req.body.details_sender);
			db.collection('travels').deleteOne( {"_id": id_parsed} );
			console.log(">> Travel removed by " + req.session.current_username);
			req.session.alert = alert.travel_remove;
			res.redirect('/main')
		} else {
			console.log(">> Details: Wrong Password");
			req.session.alert = alert.pswd_wrong;
			res.redirect('/main');
		}
	});
});

// Connect/Disconnect/Modify Account
app.get('/account',function(req, res) {
	let index_user;
	let current_time = new Time();
	let data_offers = []; let data_demands = []; let data_subscribe = [];
	if (req.session.filter === undefined) { req.session.order = 1; req.session.filter = "id_sort_start_date"; }
	let cursor = db.collection('travels').find({"travel_user" : req.session.current_username}).sort( parse_filter_and_order(req) );
	let cursor_subscribe = db.collection('travels').find( { "travel_type" : "O", "passengers.subscribers" : req.session.current_username } ).sort( parse_filter_and_order(req) );

	cursor_subscribe.forEach(function (doc_sub) {
		doc_sub.outdated_color = (current_time.is_outdated(doc_sub.start_date)) ? '#CF1020' : is_full_color(doc_sub);
		index_user = doc_sub.passengers.subscribers.lastIndexOf(req.session.current_username);
		doc_sub.tickets_nbr = doc_sub.passengers.places[index_user];
		data_subscribe.push(doc_sub);
	}, function () {
		cursor.forEach(function (doc) {
			doc.outdated_color = (current_time.is_outdated(doc.start_date)) ? '#CF1020' : is_full_color(doc);
			doc.travel_type === "O" ? data_offers.push(doc) : data_demands.push(doc);
	}, function () {
		res.render('account.html', {
			alert: Alert.send(req),
			date: current_time.setup_date_main(),
			current_first_name: req.session.current_first_name,
			current_last_name: req.session.current_last_name,
			current_email: req.session.current_email,
			current_picture: req.session.current_picture,
			is_connected: req.session.current_username,
			is_offers: data_offers.length !== 0,
			offers_list: data_offers,
			is_demands: data_demands.length !== 0,
			demands_list: data_demands,
			is_sub_offer: data_subscribe.length !== 0,
			sub_offer_list: data_subscribe,
			not_found_o: no_found_generator_html("offer"),
			not_found_d: no_found_generator_html("demand"),
			not_found_s: no_found_generator_html("subscribe"),
			order_increase: which_filter_order_selected(req, 1), order_decrease: which_filter_order_selected(req, -1),
			filter_start_when: which_filter_order_selected(req, "id_sort_start_date"), filter_start_where: which_filter_order_selected(req, "start_where"), filter_arrive_where: which_filter_order_selected(req, "arrive_where"), filter_price: which_filter_order_selected(req, "price"), filter_nbr_passengers: which_filter_order_selected(req, "nbr_passengers"), filter_user: which_filter_order_selected(req, "travel_user"), filter_post: which_filter_order_selected(req, "_id"),
		});
	})});
});

app.post('/account/check', function(req, res) {
	if (req.body.user !== "" && req.body.pswd !== "") {
		req.body.user = (req.body.user.trim()).toLowerCase();
		db.collection('users').findOne({"username": req.body.user}, (err, data_user) => {
			if (err) throw err;
			if (data_user === null) {
				console.log(">> '" + req.body.user + "' doesn't exist in the database");
				req.session.alert = alert.connect_wrong;
				res.redirect('/account');
			} else {
				if (req.body.pswd === data_user.pswd) {
					set_current_user(req, false, data_user);
					console.log(">> " + req.body.user + " is connected");
					req.session.alert = alert.connect_success;
					res.redirect('/main');
				} else {
					console.log(">> Wrong Password");
					req.session.alert = alert.connect_wrong;
					res.redirect('/account');
				}
			}
		});
	} else {
		req.session.alert = alert.missing;
		res.redirect('/account');
	}
});

app.post('/account/create', function(req, res) {
	if (req.body.create_username !== "" && req.body.create_pswd !== "" && req.body.create_first_name !== "" && req.body.create_last_name !== "" && req.body.create_email !== "") {
		req.body.create_username = (req.body.create_username.trim()).toLowerCase();
		db.collection('users').findOne( {"username" : req.body.create_username}, (err, data_user) => {
			if (err) throw err;
			if (data_user === null) {
				db.collection('users').insertOne({
					"username": req.body.create_username,
					"pswd": req.body.create_pswd,
					"first_name": req.body.create_first_name,
					"last_name": req.body.create_last_name,
					"email": req.body.create_email,
					"picture": "../images/user_neutral.png",
				});
				set_current_user(req, true);
				console.log(">> " + req.body.create_username + " has created a new account");
				req.session.alert = alert.subscribe_created;
				res.redirect('/main');
			} else {
				console.log(">> '" + req.body.create_username + "' already exist in the database");
				req.session.alert = alert.subscribe_exist;
				res.redirect('/account');
			}
		});
	} else {
		req.session.alert = alert.missing;
		res.redirect('/account');
	}
});

app.post('/account/modify/picture', function(req, res) {
	if (req.body.modify_picture_new !== "") {
		req.session.current_picture = req.body.modify_picture_new;
		req.session.alert = alert.picture_set;
	} else {
		req.session.current_picture = "../images/user_neutral.png";
		req.session.alert = alert.picture_reset;
	}
	db.collection('users').updateOne(
		{"username": req.session.current_username},
		{$set: {"picture": req.session.current_picture}}
	);
	res.redirect('/main');
});

app.post('/account/modify/fullname', function(req, res) {
	db.collection('users').findOne( {"username" : req.session.current_username}, (err, data_user) => {
	    if (err) throw err;
	    if (data_user === null) {
	    	console.log(">> Modification Error");
	    	req.session.alert = alert.error;
			res.redirect('/account');
		} else {
	    	if (req.body.modify_full_name_pswd === data_user.pswd) {
				let sender = changing_name(req, data_user, alert);
				if (sender !== null) {
					db.collection('users').updateOne(
						{"username": req.session.current_username},
						{$set: sender}
					);
				}
	    		res.redirect('/main');
			} else {
	    		console.log(">> Modification: Wrong Password");
	    		req.session.alert = alert.pswd_wrong;
	    		res.redirect('/account');
			}
		}
	});
});

app.post('/account/modify/pswd', function(req, res) {
	db.collection('users').findOne( {"username" : req.session.current_username}, (err, data_user) => {
	    if (err) throw err;
	    if (data_user === null) {
	    	console.log(">> Modification Error");
	    	req.session.alert = alert.error;
			res.redirect('/account');
		} else {
	    	if (req.body.modify_pswd_current === data_user.pswd) {
	    		db.collection('users').updateOne(
	    			{"username" : req.session.current_username},
					{$set: {"pswd" : req.body.modify_pswd_new}}
				);
	    		req.session.alert = alert.pswd_modified;
	    		res.redirect('/main');
			} else {
	    		console.log(">> Modification: Wrong Password");
	    		req.session.alert = alert.pswd_wrong;
	    		res.redirect('/account');
			}
		}
	});
});

app.post('/account/modify/email', function(req, res) {
	db.collection('users').findOne( {"username" : req.session.current_username}, (err, data_user) => {
	    if (err) throw err;
	    if (data_user === null) {
	    	console.log(">> Modification Error");
	    	req.session.alert = alert.error;
			res.redirect('/account');
		} else {
	    	if (req.body.modify_email_pswd === data_user.pswd) {
	    		db.collection('users').updateOne(
	    			{"username" : req.session.current_username},
					{$set: {"email" : req.body.modify_email_new}}
				);
	    		req.session.current_email = req.body.modify_email_new;
	    		req.session.alert = alert.email_modified;
	    		res.redirect('/main');
			} else {
	    		console.log(">> Modification: Wrong Password");
	    		req.session.alert = alert.pswd_wrong;
	    		res.redirect('/account');
			}
		}
	});
});

app.post('/account/filter', function(req, res) {
	if (req.body.filter_btn === "increase") {req.session.order = 1;}
	else if (req.body.filter_btn === "decrease") {req.session.order = -1;}
	else if (req.body.filter_btn === "start_when") {req.session.filter = "id_sort_start_date";}
	else if (req.body.filter_btn === "start_where") {req.session.filter = "start_where";}
	else if (req.body.filter_btn === "arrive_where") {req.session.filter = "arrive_where";}
	else if (req.body.filter_btn === "price") {req.session.filter = "price";}
	else if (req.body.filter_btn === "nbr_passengers") {req.session.filter = "nbr_passengers";}
	else if (req.body.filter_btn === "user") {req.session.filter = "travel_user";}
	else if (req.body.filter_btn === "post") {req.session.filter = "_id";}
	res.redirect('/account');
});

app.post('/disconnected',function(req, res) {
	reset_current_user(req);
	req.session.alert = alert.disconnect;
	res.redirect('/main');
});

// Add Travels
app.get('/adder',function(req, res) {
	if (req.session.current_username) {
		res.render('adder.html', {
			alert: Alert.send(req),
			date: new Time().setup_date_main(),
			current_first_name: req.session.current_first_name,
			current_last_name: req.session.current_last_name,
			current_email: req.session.current_email,
			current_picture: req.session.current_picture,
			is_connected: req.session.current_username
		});
	} else {
		req.session.alert = alert.connect_before;
		res.redirect('/account')
	}
});

app.post('/adder/sending',function(req, res) {
	if (req.session.current_username && req.body.start_date !== "" && req.body.start_hour !== "" && req.body.start_where !== "" && req.body.arrive_where !== "") {
		let time = new Time();
		let to_insert;
		if (req.body.offer_or_demand === "O") {
			if (req.body.price !== "" && req.body.nbr_passengers !== "") {
				to_insert = {
					_id: time.id_maker(),
					travel_type: req.body.offer_or_demand,
					start_date: req.body.start_date,
					start_hour: req.body.start_hour,
					start_where: req.body.start_where,
					arrive_where: req.body.arrive_where,
					price: parseInt(req.body.price),
					nbr_passengers: parseInt(req.body.nbr_passengers),
					nbr_actual_passengers: 0,
					passengers: {"subscribers" : [], "places" : []},
					travel_user: req.session.current_username,
					post_date: time.setup_date_travels(),
					id_sort_start_date: Time.parse_date_with_full_month_to_id(req.body.start_date) + Time.parse_hour_to_id(req.body.start_hour),
				};
			} else {
				req.session.alert = alert.missing;
				return res.redirect('/adder');
			}
		} else {
			to_insert = {
				_id: time.id_maker(),
				travel_type: req.body.offer_or_demand,
				start_date: req.body.start_date,
				start_hour: req.body.start_hour,
				start_where: req.body.start_where,
				arrive_where: req.body.arrive_where,
				travel_user: req.session.current_username,
				post_date: time.setup_date_travels(),
				id_sort_start_date: Time.parse_date_with_full_month_to_id(req.body.start_date) + Time.parse_hour_to_id(req.body.start_hour),
			};
		}
		db.collection('travels').insertOne(to_insert);
		console.log(">> Travel added by " + req.session.current_username);
		req.session.alert = alert.travel_success;
		res.redirect('/main')
	} else {
		req.session.alert = alert.missing;
		res.redirect('/adder');
	}
});
//// GET/POST SECTION - END
//
//
//// SERVER STARTUP - START
app.use(express.static('src'));
https.createServer({
	key: fs.readFileSync('./key.pem'),
	cert: fs.readFileSync('./cert.pem'),
	passphrase: 'SharingAndHelpingOthersIsGoodForCommunityLife'
}, app).listen(8080);
console.log(`HTTPS Express server started on port 8080`);
//// SERVER STARTUP - END

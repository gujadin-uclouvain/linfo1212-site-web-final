var current_tab_search_index = 1;
var current_tab_search_is_offer;

function defined_is_offer_global_variable(is_offer) {
    if (current_tab_search_is_offer === undefined) { current_tab_search_is_offer = is_offer; }
}

function choose_tab_search(id, offer_table_exist = true, demand_table_exist = true) {
    let is_offer;
    if (!offer_table_exist) { current_tab_search_is_offer = false; }
    else {
        for (let i = 0; i < 7; i++) {
            let temp_id = "offer_" + i;
            if (temp_id === id) {
                document.getElementById(temp_id).style.color = "#26a69a";
                current_tab_search_index = i;
                is_offer = true;
            } else {
                document.getElementById(temp_id).style.color = "#000000";
            }
        }
    }
    if (!demand_table_exist) { current_tab_search_is_offer = true; }
    else {
        for (let i = 0; i < 5; i++) {
            let temp_id = "demand_" + i;
            if (temp_id === id) {
                document.getElementById(temp_id).style.color = "#26a69a";
                current_tab_search_index = i;
                is_offer = false;
            } else {
                document.getElementById(temp_id).style.color = "#000000";
            }
        }
    }
    if (is_offer) {
        document.getElementById("chooser_tab_search_offer").value = id.split("_")[1];
    } else {
        document.getElementById("chooser_tab_search_demand").value = id.split("_")[1];
    }
    if (current_tab_search_is_offer !== is_offer) {
        reset_search_other_tab(current_tab_search_is_offer);
        current_tab_search_is_offer = is_offer
    }
    dynamic_search()
}

function reset_search_other_tab(is_offer) {
    var table, tr, i, type;
    type = is_offer ? "offer" : "demand";
    table = document.getElementById(type + "_table");
    tr = table.getElementsByTagName("tr");

    for (i = 0; i < tr.length; i++) {
        tr[i].style.display = "";
    }
}

function dynamic_search() {
    let filter, table, tr, td, i, txtValue, type;
    type = current_tab_search_is_offer ? "offer" : "demand";
    filter = document.getElementById("search").value.toUpperCase();
    table = document.getElementById(type + "_table");
    tr = table.getElementsByTagName("tr");

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[current_tab_search_index];
            if (td) {
                txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

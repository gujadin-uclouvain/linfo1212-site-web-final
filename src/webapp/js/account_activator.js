function switch_tab_account(id, is_connected) {
    const array_id = is_connected ? ['modify', 'offer', 'demand', 'sub_offer'] : ['connect', 'subscribe'];
    for (let index = 0; index < array_id.length; index++) {
        if (array_id[index] === id) {
            document.getElementById(id + '_tab').classList.add('active');
            document.getElementById(id + '_list').style.display = "block";
        } else {
            document.getElementById(array_id[index] + '_tab').classList.remove('active');
            document.getElementById(array_id[index] + '_list').style.display = "none";
        }
    }
}


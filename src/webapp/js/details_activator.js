function remove_travel_button() {
    const id = document.getElementById("delete_section");
    if ( id.style.display === "block" ) { id.style.display = "none"; }
    else { id.style.display = "block"; }
}

function confirm_delete() {
    return window.confirm("Êtes-vous sûr de vouloir supprimer cette annonce ?");
}
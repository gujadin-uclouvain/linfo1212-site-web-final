function mask_offer_areas(is_offer) {
    let offer_id = document.getElementById('offer_choice');
    let demand_id = document.getElementById('demand_choice');
    if (is_offer) {
        offer_id.classList.add('green', 'white-text', 'active'); offer_id.classList.remove('green-text');
        demand_id.classList.add('green-text'); demand_id.classList.remove('green', 'white-text', 'active');

        document.getElementById('offer_or_demand').value = "O";
        document.getElementById("price").required = true;
        document.getElementById("nbr_passengers").required = true;
        document.getElementById('offer_only').style.display = 'block';
    } else {
        demand_id.classList.add('green', 'white-text', 'active'); demand_id.classList.remove('green-text');
        offer_id.classList.add('green-text'); offer_id.classList.remove('green', 'white-text', 'active');


        document.getElementById('offer_or_demand').value = "D";
        document.getElementById("price").required = false;
        document.getElementById("nbr_passengers").required = false;
        document.getElementById('offer_only').style.display = 'none';
    }
}
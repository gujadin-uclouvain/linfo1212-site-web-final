//Sidenav
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: true
    });
});

//Datepicker
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, {
        showClearBtn: true,
        firstDay: 1, // Begin the Week with the Monday
        defaultDate: new Date(),
        minDate: new Date(),
        selectMonths: true, // Enable Month Selection
        selectYears: 1, // Creates a dropdown of 1 years to control year
        format:'dd mmmm yyyy',
        i18n: {
            cancel: 'Retour',
            clear: 'Réinitialiser',
            months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthsShort: ['Janv', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
            weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        },
    });
});

//Timepicker
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.timepicker');
    var instances = M.Timepicker.init(elems, {
        showClearBtn: true,
        twelveHour: false,
        i18n: {
            cancel: 'Retour',
            clear: 'Réinitialiser',
            done: 'Ok',
        },
    });
});

//Autocomplete
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.autocomplete');
    var instances = M.Autocomplete.init(elems, {
        data: belgium_cities,
        limit: 5,
        minLength: 2,
    });
});

//Scroll Bars
document.addEventListener('DOMContentLoaded', function() {
    var main_bar = document.querySelectorAll('.main_bar');
    var main_inst = M.Pushpin.init(main_bar, {
        top: 55,
        offset: 0,
    });

    var additional_bar = document.querySelectorAll('.additional_bar');
    var additional_inst = M.Pushpin.init(additional_bar, {
        top: 360,
        offset: 64,
    });

    var search_bar_main = document.querySelectorAll('.search_bar_main');
    var search_main_inst = M.Pushpin.init(search_bar_main, {
        top: 410,
        offset: 112,
    });
});

//Float Button
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, {
        hoverEnabled: false
    });
});

//Dropdown
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems, {
        coverTrigger: false,
        constrainWidth: false,
    });
});

const belgium_cities = {
"Aalbeke": null,
"Aalst": null,
"Aalter": null,
"Aarschot": null,
"Aarsele": null,
"Aartrijke": null,
"Aartselaar": null,
"Achel": null,
"Achet": null,
"Achouffe": null,
"Adegem": null,
"Adinkerke": null,
"Affligem": null,
"Afsnee": null,
"Agimont": null,
"Aiseau-Presles": null,
"Albertkanaal Ports": null,
"Alken": null,
"Alleur": null,
"Alsemberg": null,
"Alveringem": null,
"Amay": null,
"Ambleve": null,
"Ambly": null,
"Amel": null,
"Amougies": null,
"Ampsin": null,
"Andenne": null,
"Anderlecht": null,
"Anderlues": null,
"Andoy": null,
"Andrimont": null,
"Angleur": null,
"Angreau": null,
"Anhee": null,
"Anlier": null,
"Annevoie": null,
"Ans": null,
"Anseremme": null,
"Antheit": null,
"Anthisnes": null,
"Antoing": null,
"Antwerp Churchill Terminal": null,
"Antwerpen (Anvers)": null,
"Anvaing": null,
"Anzegem": null,
"Appels": null,
"Appelterre-Eichem": null,
"Arbre": null,
"Archennes": null,
"Ardooie": null,
"Arendonk": null,
"Argenteau": null,
"Arlon": null,
"Arquennes": null,
"As": null,
"Aspelare": null,
"Asper": null,
"Asse": null,
"Assebroek": null,
"Assenede": null,
"Assesse": null,
"Astene": null,
"Ath": null,
"Athis": null,
"Athus": null,
"Attenhoven": null,
"Attenrode": null,
"Attert": null,
"Attre": null,
"Aubange": null,
"Aubechies": null,
"Aubel": null,
"Auby": null,
"Auderghem (Oudergem)": null,
"Audregnies": null,
"Auvelais": null,
"Avelgem": null,
"Averbode": null,
"Awans": null,
"Awirs": null,
"Aywaille": null,
"Baal": null,
"Baardegem": null,
"Baarle-Hertog": null,
"Baasrode": null,
"Baelen": null,
"Baileux": null,
"Baillonville": null,
"Baisy-Thy": null,
"Balen": null,
"Balgerhoek": null,
"Bande": null,
"Banneux": null,
"Barchon": null,
"Baronville": null,
"Barry": null,
"Basecles": null,
"Bas-Oha": null,
"Basse-Bodeux": null,
"Bassenge (Bitsingen)": null,
"Bassevelde": null,
"Bassilly": null,
"Bastogne": null,
"Bas-Warneton": null,
"Battel": null,
"Battice": null,
"Baudour": null,
"Baugnies": null,
"Baulers": null,
"Bavegem": null,
"Bavikhove": null,
"Bazel": null,
"Beaufays": null,
"Beaumont": null,
"Beauraing": null,
"Beauvechain": null,
"Beauwelz": null,
"Beclers": null,
"Beernem": null,
"Beerse": null,
"Beersel": null,
"Beerst": null,
"Beert": null,
"Beervelde": null,
"Beez": null,
"Begijnendijk": null,
"Beho": null,
"Beigem": null,
"Bekkevoort": null,
"Belgiek": null,
"Belgrade": null,
"Bellecourt": null,
"Bellegem": null,
"Bellem": null,
"Bellevaux": null,
"Bellingen": null,
"Beloeil": null,
"Belsele": null,
"Ben-Ahin": null,
"Bende": null,
"Berchem": null,
"Berendrecht": null,
"Bergen": null,
"Bergom": null,
"Beringen": null,
"Berlaar": null,
"Berlare": null,
"Berloz": null,
"Berneau": null,
"Bernissart": null,
"Bertem": null,
"Bertogne": null,
"Bertrix": null,
"Berzee": null,
"Beselare": null,
"Bettincourt": null,
"Bevel": null,
"Bever": null,
"Beveren": null,
"Beveren Leie": null,
"Beveren-aan-den-Ijzer": null,
"Beveren-Waas": null,
"Beverlo": null,
"Beyne-Heusay": null,
"Bienne-lez-Happart": null,
"Bierbeek": null,
"Biercee": null,
"Bierges": null,
"Bierghes": null,
"Bierset": null,
"Biesme": null,
"Biesme-sous-Thuin": null,
"Bievre": null,
"Bilstain": null,
"Bilzen": null,
"Binche": null,
"Binkom": null,
"Bissegem": null,
"Bitsingen (Bassenge)": null,
"Bizet": null,
"Blaasveld": null,
"Blaimont": null,
"Blandain": null,
"Blanden": null,
"Blankenberge": null,
"Blaregnies": null,
"Blaton": null,
"Blegny": null,
"Bleharies": null,
"Blicquy": null,
"Bocholt": null,
"Boechout": null,
"Boekhoute": null,
"Boezinge": null,
"Bohan": null,
"Bois-de-Lessines": null,
"Bois-De-Villers": null,
"Bois-d'Haine": null,
"Bolland": null,
"Bomal": null,
"Bombaye": null,
"Bonheiden": null,
"Boninne": null,
"Bon-Secours": null,
"Booischot": null,
"Boom": null,
"Boorsem": null,
"Boortmeerbeek": null,
"Borchtlombeek": null,
"Borgerhout": null,
"Borgloon": null,
"Borlo": null,
"Borlon": null,
"Born": null,
"Bornem": null,
"Bornival": null,
"Borsbeek": null,
"Borsbeke": null,
"Bossiere": null,
"Bossuit": null,
"Bothey": null,
"Bouffioulx": null,
"Bouge": null,
"Bougnies": null,
"Bouillon": null,
"Bourlers": null,
"Boussoit": null,
"Boussu": null,
"Bousval": null,
"Boutersem": null,
"Bouvignes-sur-Meuse": null,
"Bouvignies": null,
"Bouwel": null,
"Bovekerke": null,
"Bovesse": null,
"Bovigny": null,
"Braffe": null,
"Braine-l'Alleud": null,
"Braine-le-Chateau": null,
"Braine-le-Comte": null,
"Braives": null,
"Brakel": null,
"Brasmenil": null,
"Brasschaat": null,
"Brecht": null,
"Bredene": null,
"Bree": null,
"Breendonk": null,
"Bressoux": null,
"Briegden": null,
"Broechem": null,
"Brucargo": null,
"Brugelette": null,
"Brugge (Bruges)": null,
"Brunehault": null,
"Brunehaut": null,
"Brussegem": null,
"Brussel (Bruxelles)": null,
"Bruyelle": null,
"Brye": null,
"Buggenhout": null,
"Buissenal": null,
"Buizingen": null,
"Bullange": null,
"Bullingen": null,
"Burcht": null,
"Burdinne": null,
"Burg-Reuland": null,
"Bury": null,
"Butgenbach": null,
"Buvingen": null,
"Buvrinnes": null,
"Buzenol": null,
"Callenelle": null,
"Calonne": null,
"Canne (Kanne)": null,
"Carnieres": null,
"Casteau": null,
"Celles": null,
"Cerexhe-Heuseux": null,
"Cerfontaine": null,
"Ceroux-Mousty": null,
"Chaineux": null,
"Champion": null,
"Champlon": null,
"Chanly": null,
"Chantemelle": null,
"Chapelle-a-Oie": null,
"Chapelle-a-Wattines": null,
"Chapelle-lez-Herlaimont": null,
"Charleroi": null,
"Charneux": null,
"Chassepierre": null,
"Chastre": null,
"Chatelet": null,
"Chatelineau": null,
"Chaudfontaine": null,
"Chaumont-Gistoux": null,
"Chaussee-Notre-Dame-Louvignies": null,
"Chenee": null,
"Cherain": null,
"Cheratte": null,
"Chercq": null,
"Chertal": null,
"Chievres": null,
"Chimay": null,
"Chiny": null,
"Chokier": null,
"Ciney": null,
"Ciply": null,
"Clabecq": null,
"Clavier": null,
"Clermont": null,
"Cognelee": null,
"Colfontaine": null,
"Comblain-au-Pont": null,
"Comines": null,
"Corbais": null,
"Corbion": null,
"Corroy-le-Chateau": null,
"Corroy-le-Grand": null,
"Couillet": null,
"Courcelles": null,
"Courriere": null,
"Court-Saint-Etienne": null,
"Couthuin": null,
"Couvin": null,
"Crisnee": null,
"Crombach": null,
"Crupet": null,
"Cuesmes": null,
"Custinne": null,
"Dadizele": null,
"Daknam": null,
"Dalhem": null,
"Damme": null,
"Dampremy": null,
"Darion": null,
"Daussoulx": null,
"Dave": null,
"Daverdisse": null,
"De Haan": null,
"De Klinge": null,
"De Panne": null,
"De Pinte": null,
"Deerlijk": null,
"Deinze": null,
"Denderbelle": null,
"Denderhoutem": null,
"Denderleeuw": null,
"Dendermonde": null,
"Denderwindeke": null,
"Dentergem": null,
"Dergneau": null,
"Dessel": null,
"Desselgem": null,
"Destelbergen": null,
"Desteldonk": null,
"Deurle": null,
"Deurne": null,
"Deux-Acren": null,
"Diegem": null,
"Diepenbeek": null,
"Diest": null,
"Diksmuide": null,
"Dilbeek": null,
"Dilsen": null,
"Dilsen-Stokkem": null,
"Dinant": null,
"Dion": null,
"Dison": null,
"Doel": null,
"Dohan": null,
"Doische": null,
"Dolembreux": null,
"Donceel": null,
"Donk": null,
"Donstiennes": null,
"Doornik (Tournai)": null,
"Doornzele": null,
"Dottignies": null,
"Dour": null,
"Dranouter": null,
"Drogenbos": null,
"Drongen": null,
"Dudzele": null,
"Duffel": null,
"Duras": null,
"Durbuy": null,
"Dworp": null,
"Eben-Emael": null,
"Ecaussinnes": null,
"Ecaussinnes-d'Enghien": null,
"Ecaussinnes-Lalaing": null,
"Edegem": null,
"Eeklo": null,
"Eernegem": null,
"Eghezee": null,
"Eigenbilzen": null,
"Eindhout": null,
"Eine": null,
"Eisden": null,
"Eke": null,
"Ekeren": null,
"Eksaarde": null,
"Eksel": null,
"Elewijt": null,
"Elingen": null,
"Ellezelles": null,
"Ellignies-Sainte-Anne": null,
"Elsenborn": null,
"Elsene (Ixelles)": null,
"Elverdinge": null,
"Elversele": null,
"Emblem": null,
"Embourg": null,
"Emptinne": null,
"Ename": null,
"Enghien": null,
"Engis": null,
"Ensival": null,
"Epinois": null,
"Eppegem": null,
"Erembodegem": null,
"Erezee": null,
"Ermeton-sur-Biert": null,
"Erneuville": null,
"Erpe-Mere": null,
"Erpent": null,
"Erpion": null,
"Erps-Kwerps": null,
"Erquelinnes": null,
"Ertvelde": null,
"Escanaffles": null,
"Esen": null,
"Esneux": null,
"Espierres (Spiere)": null,
"Esplechin": null,
"Esquelmes": null,
"Essen": null,
"Estaimbourg": null,
"Estaimpuis": null,
"Estinnes": null,
"Etalle": null,
"Ethe": null,
"Etikhove": null,
"Etterbeek": null,
"Eupen": null,
"Evegnee-Tignee": null,
"Everberg": null,
"Evere": null,
"Evergem": null,
"Eveux": null,
"Eynatten": null,
"Faimes": null,
"Falaen": null,
"Familleureux": null,
"Farciennes": null,
"Faulx-Les Tombes": null,
"Fauvillers": null,
"Fays-les-Veneurs": null,
"Fayt-lez-Manage": null,
"Feluy": null,
"Feneur": null,
"Fernelmont": null,
"Ferrieres": null,
"Fexhe-le-Haut-Clocher": null,
"Fexhe-Slins": null,
"Filot": null,
"Flamierge": null,
"Flawinne": null,
"Flemalle": null,
"Flemalle-Grande": null,
"Flemalle-Haute": null,
"Flenu": null,
"Fleron": null,
"Fleurus": null,
"Flobecq": null,
"Flone": null,
"Floree": null,
"Floreffe": null,
"Florennes": null,
"Florenville": null,
"Floriffoux": null,
"Focant": null,
"Folx-les-Caves": null,
"Fontaine-l'Eveque": null,
"Fontaine-Valmont": null,
"Fontenoy": null,
"Forchies-la-Marche": null,
"Forest (Vorst)": null,
"Forge-Philippe": null,
"Forges": null,
"Forrieres": null,
"Fortem": null,
"Fosses-la-Ville": null,
"Foy-Notre-Dame": null,
"Frameries": null,
"Framont": null,
"Francorchamps": null,
"Franiere": null,
"Frasnes": null,
"Frasnes-lez-Anvaing": null,
"Frasnes-lez-Buissenal": null,
"Frasnes-lez-Gosselies": null,
"Freylange": null,
"Froid-Chapelle": null,
"Froidfontaine": null,
"Froidmont": null,
"Froyennes": null,
"Furnaux": null,
"Gages": null,
"Gallaix": null,
"Galmaarden": null,
"Ganshoren": null,
"Gaurain-Ramecroix": null,
"Gavere": null,
"Gedinne": null,
"Geel": null,
"Geer": null,
"Geetbets": null,
"Gelbressee": null,
"Gellik": null,
"Geluveld": null,
"Geluwe": null,
"Gembloux": null,
"Gemmenich": null,
"Genappe": null,
"Genenbos": null,
"Genk": null,
"Gent (Ghent)": null,
"Gentbrugge": null,
"Genval": null,
"Geraardsbergen": null,
"Gerin": null,
"Gerpinnes": null,
"Gesves": null,
"Ghislenghien": null,
"Ghlin": null,
"Ghoy": null,
"Gibecq": null,
"Gierle": null,
"Gijverinkhove": null,
"Gijzegem": null,
"Gilly": null,
"Gingelom": null,
"Gistel": null,
"Gits": null,
"Givry": null,
"Glabais": null,
"Glabbeek": null,
"Godinne": null,
"Godsheide": null,
"Goe": null,
"Goesnes": null,
"Gooik": null,
"Gorsem": null,
"Gosselies": null,
"Gottem": null,
"Gourdinne": null,
"Goutroux": null,
"Gouvy": null,
"Gouy-lez-Pieton": null,
"Gozee": null,
"Grace-Hollogne": null,
"Grammene": null,
"Grandglise": null,
"Grand-Halleux": null,
"Grand-Leez": null,
"Grembergen": null,
"Grez-Doiceau": null,
"Grimbergen": null,
"Grimminge": null,
"Grivegnee": null,
"Grobbendonk": null,
"Groenendaal": null,
"Groot Bijgaarden": null,
"Groot-Bijgaarden": null,
"Grote-Brogel": null,
"Grote-Spouwen": null,
"Grune": null,
"Guigoven": null,
"Gullegem": null,
"Gutschoven": null,
"Haacht": null,
"Haaltert": null,
"Haasdonk": null,
"Haasrode": null,
"Habay-la-Neuve": null,
"Habay-la-Vieille": null,
"Habergy": null,
"Haccourt": null,
"Hacquegnies": null,
"Haillot": null,
"Haine-Saint-Pierre": null,
"Hal": null,
"Halanzy": null,
"Halen": null,
"Halle": null,
"Halleux": null,
"Halluin": null,
"Halma": null,
"Ham": null,
"Hamme": null,
"Hamoir": null,
"Hamois": null,
"Hamont": null,
"Hamont-Achel": null,
"Hamsehoeven": null,
"Ham-sur-Heure-Nalinnes": null,
"Ham-sur-Sambre": null,
"Handzame": null,
"Hannut": null,
"Hansbeke": null,
"Harchies": null,
"Harelbeke": null,
"Haren": null,
"Harmignies": null,
"Harveng": null,
"Harze": null,
"Hasselt": null,
"Hastiere-par-dela": null,
"Haut-Ittre": null,
"Hautrage": null,
"Havay": null,
"Havelange": null,
"Havenne": null,
"Havinnes": null,
"Havre": null,
"Hechtel": null,
"Hechtel-Eksel": null,
"Heer": null,
"Heers": null,
"Heestert": null,
"Heffen": null,
"Heindonk": null,
"Heist": null,
"Heist-op-den-Berg": null,
"Hekelgem": null,
"Helecine": null,
"Helkijn": null,
"Hemelveerdegem": null,
"Hemiksem": null,
"Henri-Chapelle": null,
"Hensies": null,
"Heppen": null,
"Heppenbach": null,
"Heppignies": null,
"Herbeumont": null,
"Herdersem": null,
"Herent": null,
"Herentals": null,
"Herenthout": null,
"Hergenrath": null,
"Herinnes": null,
"Herk-de-Stad": null,
"Hermalle-sous-Argenteau": null,
"Hermalle-sous-Huy": null,
"Hermeton-sur-Meuse": null,
"Herne": null,
"Heron": null,
"Herquegies": null,
"Herseaux": null,
"Herselt": null,
"Herstal": null,
"Herstappe": null,
"Hertain": null,
"Herve": null,
"Herzele": null,
"Heule": null,
"Heultje": null,
"Heure-le-Romain": null,
"Heusden": null,
"Heusden-Zolder": null,
"Heusy": null,
"Heuvelland": null,
"Hever": null,
"Heverlee": null,
"Heyd": null,
"Hillegem": null,
"Hingene": null,
"Hives": null,
"Hoboken": null,
"Hodeige": null,
"Hody": null,
"Hoegaarden": null,
"Hoeilaart": null,
"Hoelbeek": null,
"Hoeleden": null,
"Hoepertingen": null,
"Hoeselt": null,
"Hoevenen": null,
"Hofstade": null,
"Hognoul": null,
"Hollain": null,
"Hollebeke": null,
"Hollogne-sur-Geer": null,
"Holsbeek": null,
"Hombourg": null,
"Honnelles": null,
"Hooglede": null,
"Hoogstraten": null,
"Horebeke": null,
"Hornu": null,
"Hotton": null,
"Houdemont": null,
"Houdeng-Aimeries": null,
"Houdeng-Goegnies": null,
"Houffalize": null,
"Hour": null,
"Hourpes": null,
"Housse": null,
"Houtaing": null,
"Houtain-le-Val": null,
"Houtain-Saint-Simeon": null,
"Houthalen": null,
"Houthalen-Helchteren": null,
"Houthem": null,
"Houthulst": null,
"Houtvenne": null,
"Houx": null,
"Houyet": null,
"Hove": null,
"Hoves": null,
"Howardries": null,
"Huizingen": null,
"Huldenberg": null,
"Hulshout": null,
"Hulste": null,
"Humbeek": null,
"Hun": null,
"Huy": null,
"Hyon": null,
"Ichtegem": null,
"Iddergem": null,
"Idegem": null,
"Ieper": null,
"Incourt": null,
"Ingelmunster": null,
"Ingooigem": null,
"Irchonwelz": null,
"Isieres": null,
"Isnes": null,
"Itegem": null,
"Itter (Ittre)": null,
"Itterbeek": null,
"Ittre (Itter)": null,
"Ivoz-Ramet": null,
"Ixelles (Elsene)": null,
"Izegem": null,
"Izenberge": null,
"Izier": null,
"Jabbeke": null,
"Jalhay": null,
"Jallet": null,
"Jambes": null,
"Jamoigne": null,
"Java": null,
"Jemappes": null,
"Jemeppe": null,
"Jemeppe-sur-Sambre": null,
"Jette": null,
"Jodoigne": null,
"Jumet": null,
"Jupille-sur-Meuse": null,
"Juprelle": null,
"Jurbise": null,
"Juseret": null,
"Kaaskerke": null,
"Kachtem": null,
"Kaggevinne": null,
"Kain": null,
"Kalken": null,
"Kallo": null,
"Kalmthout": null,
"Kampenhout": null,
"Kanne (Canne)": null,
"Kapellen": null,
"Kapelle-op-den-Bos": null,
"Kaprijke": null,
"Kaster": null,
"Kasterlee": null,
"Kaulille": null,
"Keerbergen": null,
"Kemmel": null,
"Kemzeke": null,
"Kerkhove": null,
"Kerkhoven": null,
"Kerksken": null,
"Kermt": null,
"Kerniel": null,
"Kersbeek-Miskom": null,
"Kessel": null,
"Kessel-Lo": null,
"Kesselt": null,
"Kessenich": null,
"Kester": null,
"Kettenis": null,
"Kieldrecht": null,
"Kinrooi": null,
"Klein Veerle": null,
"Klein Willebroek": null,
"Kleine-Spouwen": null,
"Klerken": null,
"Kluisbergen": null,
"Knesselaere": null,
"Knesselare": null,
"Knokke/Het Zoute": null,
"Knokke-Heist": null,
"Kobbegem": null,
"Koekelare": null,
"Koekelberg": null,
"Koersel": null,
"Koksijde": null,
"Koningshooikt": null,
"Kontich": null,
"Kooigem": null,
"Koolskamp": null,
"Korbeek-Lo": null,
"Kortemark": null,
"Kortenaken": null,
"Kortenberg": null,
"Kortessem": null,
"Kortrijk": null,
"Kozen": null,
"Kraainem": null,
"Krombeke": null,
"Kruibeke": null,
"Kruishoutem": null,
"Kuringen": null,
"Kuurne": null,
"Kwaadmechelen": null,
"La Bouverie": null,
"La Bruyere": null,
"La Calamine": null,
"La Glanerie": null,
"La Hulpe": null,
"La Louviere": null,
"La Plante": null,
"La Roche-en-Ardenne": null,
"Laakdal": null,
"Laarne": null,
"Labuissiere": null,
"Lacuisine": null,
"Ladeuze": null,
"Lamain": null,
"Lambermont": null,
"Lambusart": null,
"Lanaken": null,
"Lanaye (Ternaaien)": null,
"Landegem": null,
"Landelies": null,
"Landen": null,
"Laneffe": null,
"Langdorp": null,
"Langelede": null,
"Langemark": null,
"Langemark-Poelkapelle": null,
"Langerbrugge": null,
"Langerlo": null,
"Lanklaar": null,
"Lanquesaint": null,
"Laplaigne": null,
"Larum": null,
"Lasne": null,
"Lasne-Chapelle-Saint-Lambert": null,
"Latinne": null,
"Latour": null,
"Lauwe": null,
"Lavacherie": null,
"Le Roeulx": null,
"Lebbeke": null,
"Lede": null,
"Ledeberg": null,
"Ledegem": null,
"Leefdaal": null,
"Leernes": null,
"Leest": null,
"Leffinge": null,
"Leglise": null,
"Leisele": null,
"Lembeek": null,
"Lembeke": null,
"Lendelede": null,
"Lennik": null,
"Lens": null,
"Leopoldsburg": null,
"Les Bulles": null,
"Les-Bons-Villers": null,
"l'Escailliere": null,
"Lesdain": null,
"Lessines": null,
"Lesterny": null,
"Lesve": null,
"Letterhoutem": null,
"Leuven": null,
"Leuze-en-Hainaut": null,
"Leval-Trahegnies": null,
"Liberchies": null,
"Libin": null,
"Libramont-Chevigny": null,
"Lichtaart": null,
"Lichtervelde": null,
"Liedekerke": null,
"Liefkenshoek": null,
"Liege": null,
"Lier": null,
"Lierneux": null,
"Liers": null,
"Ligne": null,
"Ligneuville": null,
"Ligney": null,
"Lille": null,
"Lillo": null,
"Lillois-Witterzee": null,
"Limbourg": null,
"Limelette": null,
"Lincent": null,
"Linden": null,
"Linkebeek": null,
"Lint": null,
"Linter": null,
"Lisogne": null,
"Lissewege": null,
"Lives-sur-Meuse": null,
"Lixhe": null,
"Lo": null,
"Lobbes": null,
"Lochristi": null,
"Lodelinsart": null,
"Loenhout": null,
"Loker": null,
"Lokeren": null,
"Lombardsijde": null,
"Lommel": null,
"Lommersweiler": null,
"Lompret": null,
"Lomprez": null,
"Loncin": null,
"Londerzeel": null,
"Longchamps": null,
"Longueville": null,
"Lontzen": null,
"Loppem": null,
"Lo-Reninge": null,
"Lot": null,
"Loupoigne": null,
"Louvain-la-Neuve": null,
"Louveigne": null,
"Lovendegem": null,
"Loyers": null,
"Lozen": null,
"Lubbeek": null,
"Luingne": null,
"Lummen": null,
"Lustin": null,
"Luttre": null,
"Maarkedal": null,
"Maaseik": null,
"Maasmechelen": null,
"Mabompre": null,
"Machelen": null,
"Macon": null,
"Macquenoise": null,
"Maffe": null,
"Maffle": null,
"Magnee": null,
"Maillen": null,
"Mainvault": null,
"Maisieres": null,
"Maissin": null,
"Maizeret": null,
"Maldegem": null,
"Malderen": null,
"Malle": null,
"Malmedy": null,
"Malonne": null,
"Manage": null,
"Manhay": null,
"Mannekensvere": null,
"Maransart": null,
"Marbais": null,
"Marbehan": null,
"Marche-en-Famenne": null,
"Marche-les-Dames": null,
"Marche-Lez-Ecaussinnes": null,
"Marche-lez-Ecaussinnes": null,
"Marchienne-au-Pont": null,
"Marchin": null,
"Marcinelle": null,
"Mariakerke": null,
"Marienbourg": null,
"Marke": null,
"Marly": null,
"Marquain": null,
"Martelange": null,
"Martenslinde": null,
"Martouzin-Neuville": null,
"Masbourg": null,
"Massenhoven": null,
"Maubray": null,
"Maulde": null,
"Maurage": null,
"Mazy": null,
"Mean": null,
"Mechelen": null,
"Meensel-Kiezegem": null,
"Meer": null,
"Meerbeek": null,
"Meerbeke": null,
"Meerdonk": null,
"Meerhout": null,
"Meerle": null,
"Meeuwen-Gruitrode": null,
"Meigem": null,
"Meilegem": null,
"Meise": null,
"Meix-devant-Virton": null,
"Meldert": null,
"Melen": null,
"Melin": null,
"Melle": null,
"Melles": null,
"Mellet": null,
"Melsbroek": null,
"Melsele": null,
"Melsen": null,
"Mendonk": null,
"Menen": null,
"Merbes-le-Chateau": null,
"Merchtem": null,
"Mere": null,
"Merelbeke": null,
"Merendree": null,
"Merkem": null,
"Merksem": null,
"Merksplas": null,
"Merlemont": null,
"Mesen": null,
"Meslin-l'Eveque": null,
"Mespelare": null,
"Messancy": null,
"Messelbroek": null,
"Mesvin": null,
"Mettet": null,
"Meulebeke": null,
"Meux": null,
"Mevergnies-lez-Lens": null,
"Micheroux": null,
"Middelburg": null,
"Middelkerke": null,
"Miecret": null,
"Mielen-boven-Aalst": null,
"Milmort": null,
"Minderhout": null,
"Modave": null,
"Moelingen": null,
"Moen": null,
"Moerbeke-Waas": null,
"Moerbrugge": null,
"Moerdijk": null,
"Moerkerke": null,
"Moerzeke": null,
"Moeskroen (Mouscron)": null,
"Mohiville": null,
"Moignelee": null,
"Mol": null,
"Molenbeek-Saint-Jean (Sint-Jans-Molenbeek)": null,
"Molenbeek-Wersbeek": null,
"Molenstede": null,
"Molinfaing": null,
"Mollem": null,
"Momignies": null,
"Monceau-Imbrechies": null,
"Monceau-sur-Sambre": null,
"Mons": null,
"Monsin": null,
"Mont": null,
"Mont-de-L'Enclus": null,
"Montegnee": null,
"Montignies-le-Tilleul": null,
"Montignies-Sur-Roc": null,
"Montignies-sur-Sambre": null,
"Montleban": null,
"Montroeul-au-Bois": null,
"Mont-Saint-Aubert": null,
"Mont-Sainte-Aldegonde": null,
"Mont-Saint-Guibert": null,
"Mont-sur-Marchienne": null,
"Montzen": null,
"Moorsel": null,
"Moorsele": null,
"Moorslede": null,
"Morialme": null,
"Morlanwelz-Mariemont": null,
"Mornimont": null,
"Mortroux": null,
"Mortsel": null,
"Moulbaix": null,
"Mourcourt": null,
"Mouscron (Moeskroen)": null,
"Moustier": null,
"Muizen": null,
"Muno": null,
"Munsterbilzen": null,
"Musson": null,
"Naast": null,
"Nameche": null,
"Namur": null,
"Nandrin": null,
"Naninne": null,
"Nassogne": null,
"Natoye": null,
"Nazareth": null,
"Nederboelare": null,
"Nederokkerzeel": null,
"Neder-Over-Heembeek": null,
"Nederzwalm-Hermelgem": null,
"Neerharen": null,
"Neerlanden": null,
"Neeroeteren": null,
"Neerpelt": null,
"Neffe": null,
"Neigem": null,
"Nessonvaux": null,
"Neufchateau": null,
"Neufmaison": null,
"Neufvilles": null,
"Neu-Moresnet": null,
"Neupre": null,
"Neuville-en-Condroz": null,
"Neuville-sous-Huy": null,
"Nevele": null,
"Nidrum": null,
"Niel": null,
"Nieuwenhove": null,
"Nieuwerkerken": null,
"Nieuwkerken-Waas": null,
"Nieuwpoort": null,
"Nieuwrode": null,
"Nijlen": null,
"Nimy": null,
"Ninove": null,
"Nivelles": null,
"Noeveren": null,
"Noirefontaine": null,
"Nokere": null,
"Nollevaux": null,
"Noorderwijk": null,
"Nossegem": null,
"Nouvelles": null,
"Nukerke": null,
"Obigies": null,
"Obourg": null,
"Oedelem": null,
"Oelegem": null,
"Oeselgem": null,
"Oeudeghien": null,
"Oevel": null,
"Ogy": null,
"Ohain": null,
"Ohey": null,
"Oisquercq": null,
"Okegem": null,
"Olen": null,
"Ollignies": null,
"Olmen": null,
"Olne": null,
"Olsene": null,
"Omal": null,
"Ombret-Rawsa": null,
"Onhaye": null,
"Onkerzele": null,
"Onze-Lieve-Vrouw-Waver": null,
"Ooigem": null,
"Oostakker": null,
"Oosteeklo": null,
"Oostende (Ostend)": null,
"Oosterlo": null,
"Oosterzele": null,
"Oostham": null,
"Oostkamp": null,
"Oostkerke": null,
"Oostmalle": null,
"Oostrozebeke": null,
"Oostvleteren": null,
"Oostwinkel": null,
"Opglabbeek": null,
"Ophain-Bois-Seigneur-Isaac": null,
"Ophasselt": null,
"Ophoven": null,
"Opitter": null,
"Opont": null,
"Opwijk": null,
"Orcq": null,
"Oreye": null,
"Ormeignies": null,
"Orp-Jauche": null,
"Orroir": null,
"Ortho": null,
"Ostiches": null,
"Otegem": null,
"Oteppe": null,
"Ottignies-Louvain-la Neuve": null,
"Oudegem": null,
"Oudenaarde": null,
"Oudenaken": null,
"Oudenburg": null,
"Oudergem (Auderghem)": null,
"Oud-Heverlee": null,
"Oud-Turnhout": null,
"Ouffet": null,
"Ougree": null,
"Oupeye": null,
"Outer": null,
"Outgaarden": null,
"Outrijve": null,
"Overboelare": null,
"Overijse": null,
"Overpelt": null,
"Paal": null,
"Paifve": null,
"Paliseul": null,
"Pamel": null,
"Papignies": null,
"Parike": null,
"Passendale": null,
"Pecq": null,
"Peer": null,
"Peisegem": null,
"Pellenberg": null,
"Pepingen": null,
"Pepinster": null,
"Perk": null,
"Peronnes": null,
"Peronnes/Binche": null,
"Peruwelz": null,
"Perwez": null,
"Pesche": null,
"Petegem": null,
"Petit-Lanaye": null,
"Petit-Rechain": null,
"Petit-Roeulx-lez-Nivelles": null,
"Petit-Thier": null,
"Philippeville": null,
"Pijp Tabak": null,
"Pipaix": null,
"Pittem": null,
"Plainevaux": null,
"Plassendale": null,
"Ploegsteert": null,
"Plombieres": null,
"Poederlee": null,
"Pollare": null,
"Pollinkhove": null,
"Pommeroeul": null,
"Pondrome": null,
"Pont-a-Celles": null,
"Pont-de-Loup": null,
"Poperinge": null,
"Poppel": null,
"Postel": null,
"Pottes": null,
"Poulseur": null,
"Profondeville": null,
"Proven": null,
"Pulderbos": null,
"Pulle": null,
"Purnode": null,
"Putte": null,
"Puurs": null,
"Quaregnon": null,
"Quartes": null,
"Quenast": null,
"Quevaucamps": null,
"Quevy": null,
"Quievrain": null,
"Rachecourt": null,
"Raeren": null,
"Ragnies": null,
"Rahier": null,
"Ramegnies": null,
"Ramegnies-Chin": null,
"Ramillies": null,
"Ramsdonk": null,
"Ramskapelle": null,
"Ransart": null,
"Ranst": null,
"Ravels": null,
"Rebaix": null,
"Rebecq": null,
"Recogne": null,
"Reet": null,
"Rekem": null,
"Rekkem": null,
"Relegem": null,
"Remersdaal": null,
"Remicourt": null,
"Rendeux": null,
"Reninge": null,
"Renory": null,
"Reppel": null,
"Ressaix": null,
"Ressegem": null,
"Retie": null,
"Retinne": null,
"Richelle": null,
"Rieme": null,
"Riemst": null,
"Riezes": null,
"Rijkevorsel": null,
"Rijmenam": null,
"Rillaar": null,
"RIS Inland waterways": null,
"Riviere": null,
"Rixensart": null,
"Rochefort": null,
"Rocherath": null,
"Rocourt": null,
"Roesbrugge": null,
"Roesbrugge-Haringe": null,
"Roeselare": null,
"Rognee": null,
"Rollegem": null,
"Romsee": null,
"Rongy": null,
"Ronquieres": null,
"Ronse": null,
"Ronsele": null,
"Roosdaal": null,
"Roselies": null,
"Rosieres": null,
"Rossignol": null,
"Rotem": null,
"Rotselaar": null,
"Roucourt": null,
"Rouvroy": null,
"Roux": null,
"Ruddervoorde": null,
"Ruien": null,
"Ruisbroek": null,
"Ruiselede": null,
"Rulles": null,
"Rumbeke": null,
"Rumes": null,
"Rumillies": null,
"Rummen": null,
"Rumst": null,
"Runkelen": null,
"Rupelmonde": null,
"'s Gravenvoeren": null,
"Saint-Amand": null,
"Saint-Denis": null,
"Sainte-Cecile": null,
"Sainte-Ode": null,
"Saintes": null,
"Saint-Georges": null,
"Saint-Georges-sur-Meuse": null,
"Saint-Ghislain": null,
"Saint-Gilles (Sint-Gillis)": null,
"Saint-Hubert": null,
"Saint-Josse-ten-Noode (Sint-Joost-ten-Node)": null,
"Saint-Leger": null,
"Saint-Marc": null,
"Saint-Mard": null,
"Saint-Nicolas": null,
"Saint-Remy": null,
"Saint-Sauveur": null,
"Saint-Servais": null,
"Saint-Symphorien": null,
"Saint-Vincent": null,
"Saint-Vith": null,
"Saive": null,
"Salles": null,
"Salzinnes": null,
"Sambreville": null,
"Samree": null,
"Samson": null,
"Sankt Vith": null,
"Sars-la-Buissiere": null,
"Sart Les Spa": null,
"Sas-Slijkens": null,
"Sautin": null,
"Schaarbeek": null,
"Schaarbeek (Schaerbeek)": null,
"Schalkhoven": null,
"Schaltin": null,
"Scheepsdale": null,
"Schelderode": null,
"Schelle": null,
"Schellebelle": null,
"Schendelbeke": null,
"Schepdaal": null,
"Scherpenheuvel-Zichem": null,
"Schilde": null,
"Schoenberg": null,
"Schoonaarde": null,
"Schore": null,
"Schorisse": null,
"Schoten": null,
"Schriek": null,
"Sclaigneaux": null,
"Sclayn": null,
"Sclessin": null,
"Scy": null,
"Seilles": null,
"Selange": null,
"Seloignes": null,
"Seneffe": null,
"Sensenruth": null,
"Seny": null,
"Seraing": null,
"Serskamp": null,
"Serville": null,
"'s-Gravenwezel": null,
"Sijsele": null,
"Silly": null,
"Sinaai": null,
"Sint Gillis Waas": null,
"Sint Joris Winge": null,
"Sint Katelijne Waver": null,
"Sint Pieters Leeuw": null,
"Sint-Agatha-Berchem (Berchem-Sainte-Agathe)": null,
"Sint-Amands": null,
"Sint-Amandsberg": null,
"Sint-Andries": null,
"Sint-Baafs-Vijve": null,
"Sint-Denijs": null,
"Sint-Denijs-Westrem": null,
"Sint-Eloois-Vijve": null,
"Sint-Eloois-Winkel": null,
"Sint-Genesius-Rode (Rhode-Saint-Genese)": null,
"Sint-Gillis (Saint-Gilles)": null,
"Sint-Gillis-Waas": null,
"Sint-Huibrechts-Lille": null,
"Sint-Jacobs-Kapelle": null,
"Sint-Jan-in-Eremo": null,
"Sint-Jans-Molenbeek (Molenbeek-Saint-Jean)": null,
"Sint-Job-in-'t-Goor": null,
"Sint-Joost-ten-Node (Saint-Josse-ten-Noode)": null,
"Sint-Joris": null,
"Sint-Joris-Weert": null,
"Sint-Jozef-Olen": null,
"Sint-Katelijne-Waver": null,
"Sint-Katherina-Lombeek": null,
"Sint-Kruis": null,
"Sint-Kruis-Winkel": null,
"Sint-Lambrechts-Woluwe (Woluwe-Saint-Lambert)": null,
"Sint-Laureins": null,
"Sint-Lenaarts": null,
"Sint-Lievens-Esse": null,
"Sint-Lievens-Houtem": null,
"Sint-Margriete": null,
"Sint-Martens-Bodegem": null,
"Sint-Martens-Latem": null,
"Sint-Martens-Lierde": null,
"Sint-Michiels": null,
"Sint-Niklaas": null,
"Sint-Pauwels": null,
"Sint-Pieers-Leeuw": null,
"Sint-Pieters-Kapelle": null,
"Sint-Pieters-Leeuw": null,
"Sint-Pieters-Voeren": null,
"Sint-Pieters-Woluwe (Woluwe-Saint-Pierre)": null,
"Sint-Stevens-Woluwe": null,
"Sint-Truiden": null,
"Sippenaeken": null,
"Sirault": null,
"Sivry-Rance": null,
"Sleidinge": null,
"Slijpe": null,
"Smeerebbe-Vloerzegem": null,
"Smeermaas": null,
"Snaaskerke": null,
"Soignies": null,
"Soiron": null,
"Solre-sur-Sambre": null,
"Sombreffe": null,
"Somme-Leuze": null,
"Sommiere": null,
"Somzee": null,
"Sorinne-la-Longue": null,
"Sorinnes": null,
"Soudromont": null,
"Soumagne": null,
"Souvret": null,
"Soy": null,
"Soye": null,
"Spa": null,
"Spalbeek": null,
"Spiennes": null,
"Spiere (Espierres)": null,
"Sprimont": null,
"Stabroek": null,
"Staden": null,
"Stambruges": null,
"Stasegem": null,
"Statte": null,
"Stave": null,
"Stavele": null,
"Stavelot": null,
"Steenbrugge": null,
"Steendorp": null,
"Steenhuffel": null,
"Steenkerke": null,
"Steenokkerzeel": null,
"Stekene": null,
"Stembert": null,
"Sterpenich": null,
"Sterrebeek": null,
"Stevensvennen": null,
"Stevoort": null,
"Stokrooie": null,
"Stoumont": null,
"Stree": null,
"Strepy-Bracquegnies": null,
"Strombeek-Bever": null,
"Suarlee": null,
"Suxy": null,
"Tailles": null,
"Taintignies": null,
"Tamines": null,
"Tarcienne": null,
"Tavier": null,
"Tavigny": null,
"Tellin": null,
"Templeuve": null,
"Temploux": null,
"Temse": null,
"Tenneville": null,
"Teralfene": null,
"Terdonk": null,
"Tergnee": null,
"Terhagen": null,
"Termes": null,
"Ternaaien (Lanaye)": null,
"Ternat": null,
"Tertre": null,
"Tervuren": null,
"Tessenderlo": null,
"Testelt": null,
"Teuven": null,
"Theux": null,
"Thieu": null,
"Thieulain": null,
"Thieusies": null,
"Thimister": null,
"Thimister-Clermont": null,
"Thimougies": null,
"Thines": null,
"Thommen": null,
"Thon Samson": null,
"Thorembais-les-Beguines": null,
"Thorembais-Saint-Trond": null,
"Thorntonbank": null,
"Thuillies": null,
"Thuin": null,
"Thulin": null,
"Thumaide": null,
"Thy-le-Chateau": null,
"Thynes": null,
"Tiegem": null,
"Tielen": null,
"Tielrode": null,
"Tielt": null,
"Tielt-Winge": null,
"Tienen": null,
"Tihange": null,
"Tildonk": null,
"Tilff": null,
"Tilleur": null,
"Tilly": null,
"Tinlot": null,
"Tintigny": null,
"Tisselt": null,
"Tohogne": null,
"Tongeren": null,
"Tongerlo": null,
"Tongre-Notre-Dame": null,
"Torhout": null,
"Tourinne": null,
"Tourinnes-St-Lambert": null,
"Tournai (Doornik)": null,
"Tournay": null,
"Tournebride": null,
"Tourpes": null,
"Trazegnies": null,
"Tremelo": null,
"Trois-Ponts": null,
"Trooz": null,
"Tubize": null,
"Turnhout": null,
"Uccle (Ukkel)": null,
"Ucimont": null,
"Uikhoven": null,
"Uitbergen": null,
"Ukkel (Uccle)": null,
"Ulbeek": null,
"Ursel": null,
"Vaalbeek": null,
"Val Saint-Lambert": null,
"Vance": null,
"Varendonk": null,
"Vaulx": null,
"Vaux-sous-Chevremont": null,
"Vaux-sur-Sure": null,
"Vedrin": null,
"Veerle": null,
"Velaine-sur-Sambre": null,
"Veldegem": null,
"Veldwezelt": null,
"Velm": null,
"Verbrande Brug": null,
"Verlaine": null,
"Verrebroek": null,
"Verviers": null,
"Veurne": null,
"Vezin": null,
"Vezon": null,
"Vichte": null,
"Vielsalm": null,
"Viersel": null,
"Viesville": null,
"Vieux-Genappe": null,
"Ville-Pommeroeul": null,
"Villerot": null,
"Villers-aux-Tours": null,
"Villers-devant-Orval": null,
"Villers-la-Ville": null,
"Villers-le-Bouillet": null,
"Villers-Notre-Dame": null,
"Villers-Saint-Amand": null,
"Villers-Sainte-Gertrude": null,
"Villers-Saint-Ghislain": null,
"Villers-Saint-Simeon": null,
"Ville-sur-Haine": null,
"Vilvoorde": null,
"Vinalmont": null,
"Vinderhoute": null,
"Virelles": null,
"Virginal-Samme": null,
"Viroinval": null,
"Virton": null,
"Vise": null,
"Vivegnis": null,
"Vivy": null,
"Vlamertinge": null,
"Vleteren": null,
"Vlezenbeek": null,
"Vliermaal": null,
"Vliermaalroot": null,
"Vlissegem": null,
"Voeren": null,
"Vollezele": null,
"Voormezele": null,
"Voroux-lez-Liers": null,
"Vorselaar": null,
"Vorst (Forest)": null,
"Vosselaar": null,
"Vottem": null,
"Vrasene": null,
"Vremde": null,
"Vresse-sur-Semois": null,
"Vroenhoven": null,
"Waanrode": null,
"Waarbeke": null,
"Waardamme": null,
"Waarloos": null,
"Waarmaarde": null,
"Waarschoot": null,
"Waasmont": null,
"Waasmunster": null,
"Wachtebeke": null,
"Wadelincourt": null,
"Wagnelee": null,
"Waimes": null,
"Wakken": null,
"Walcourt": null,
"Walem": null,
"Walhain": null,
"Walhorn": null,
"Walsbets": null,
"Walshoutem": null,
"Wambeek": null,
"Wandre": null,
"Wanfercee-Baulet": null,
"Wanlin": null,
"Wannebecq": null,
"Wannegem-Lede": null,
"Wanze": null,
"Warchin": null,
"Warcoing": null,
"Waregem": null,
"Waremme": null,
"Warnant": null,
"Warneton": null,
"Warsage": null,
"Wasmes-Audemez-Briffoeil": null,
"Wasseiges": null,
"Waterland-Oudeman": null,
"Waterloo": null,
"Waterloos": null,
"Watermael-Boitsfort (Watermaal-Bosvoorde)": null,
"Watervliet": null,
"Watou": null,
"Waudrez": null,
"Waulsort": null,
"Wauthier-Braine": null,
"Waver": null,
"Wavre": null,
"Ways": null,
"Wechelderzande": null,
"Weelde": null,
"Weerde": null,
"Weert": null,
"Wegnez": null,
"Weillen": null,
"Welkenraedt": null,
"Welle": null,
"Wellen": null,
"Wellin": null,
"Wemmel": null,
"Wepion": null,
"Werchter": null,
"Werken": null,
"Wervik": null,
"Wespelaar": null,
"Westende": null,
"Westerlo": null,
"Westkapelle": null,
"Westmalle": null,
"Westmeerbeek": null,
"Westouter": null,
"Westrem": null,
"Westrozebeke": null,
"Wetteren": null,
"Wevelgem": null,
"Wezel": null,
"Wezemaal": null,
"Wezembeek-Oppem": null,
"Wezeren": null,
"Wibrin": null,
"Wichelen": null,
"Wiekevorst": null,
"Wielsbeke": null,
"Wierde": null,
"Wiers": null,
"Wiesme": null,
"Wieze": null,
"Wihogne": null,
"Wijgmaal": null,
"Wijnegem": null,
"Wijtschate": null,
"Wilderen": null,
"Willaupuis": null,
"Willebroek": null,
"Willemeau": null,
"Wilrijk": null,
"Wilsele": null,
"Wilskerke": null,
"Wimmertingen": null,
"Wingene": null,
"Wintershoven": null,
"Winterslag": null,
"Wintham": null,
"Woluwe-Saint-Lambert (Sint-Lambrechts-Woluwe)": null,
"Woluwe-Saint-Pierre (Sint-Pieters-Woluwe)": null,
"Wolvertem": null,
"Wommelgem": null,
"Wondelgem": null,
"Wortegem": null,
"Wortegem-Petegem": null,
"Wortel": null,
"Wulpen": null,
"Wulvergem": null,
"Wuustwezel": null,
"Xhendelesse": null,
"Xhendremael": null,
"Xhoris": null,
"Yvoir": null,
"Zandbergen": null,
"Zandhoven": null,
"Zandvliet": null,
"Zandvoorde": null,
"Zarren": null,
"Zaventem": null,
"Zedelgem": null,
"Zeebrugge": null,
"Zele": null,
"Zelem": null,
"Zellik": null,
"Zelzate": null,
"Zemst": null,
"Zevergem": null,
"Zichem": null,
"Zillebeke": null,
"Zingem": null,
"Zoersel": null,
"Zolder": null,
"Zomergem": null,
"Zonhoven": null,
"Zonnebeke": null,
"Zottegem": null,
"Zoutenaaie": null,
"Zoutleeuw": null,
"Zuen (Zuun)": null,
"Zuidschote": null,
"Zuienkerke": null,
"Zulte": null,
"Zutendaal": null,
"Zuun (Zuen)": null,
"Zwalm": null,
"Zwevegem": null,
"Zwevezele": null,
"Zwijnaarde": null,
"Zwijndrecht": null,
};

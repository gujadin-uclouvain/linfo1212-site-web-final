function show_and_hide(id, show_default=true) {
    var x = document.getElementById(id);
    show_default === false ?
        (x.style.display === "none" ? x.style.display = "block" : x.style.display = "none") :
        (x.style.display === "block" ? x.style.display = "none" : x.style.display = "block")
}

function disconnect_button() {
    if (window.confirm("Êtes-vous sûr de vouloir vous déconnecter ?")) {
        document.forms['disconnect_form'].submit();
    }
}

function filter_button(type) {
    let id = document.getElementById('filter_btn');
    id.value = type;
    document.forms['filter_form'].submit();
}

function confirm_action() {
    return window.confirm("Êtes-vous sûr de vouloir envoyer les données rentrées ?");
}

function generate_id(id, type) {
    if (id === "") {
        let data = document.querySelectorAll('.' + type);
        let counter = 0;
        Array.prototype.forEach.call(data, function (elements, index) {
            elements.id = counter++;
        });
    }
}

function send_data_button(id, type) {
    let result = document.getElementById(id).innerHTML;
    result = result.split("<td>");
    result = result[result.length - 1].split("</td>")[0];
    if (type === "details_demand_line") {
        document.getElementById("details_demand_sender").value = result;
        document.forms['details_demand_form'].submit();
    } else if (type === "details_offer_line") {
        document.getElementById("details_offer_sender").value = result;
        document.forms['details_offer_form'].submit();
    } else if (type === "details_sub_offer_line") {
        document.getElementById("details_sub_offer_sender").value = result;
        document.forms['details_sub_offer_form'].submit();
    }
}


## **Projet Final** => **HTML - CSS - JavaScript - NodeJS - MongoDB (Groupe P)**
---

## **MEMBERS:**
- Guillaume Jadin
- Hugo De Georgi
- Ayman Alaoui Mhammedi

## **COMMANDS:**
**NodeJS Modules:**

- *npm install express consolidate hogan express-session body-parser https fs mongodb*

**Launch MongoDB Database:**

- *mongod --dbpath "[PATH OF THE PROJECT]/src/database/"*

**Launch Server:**

- *cd ./src/server*

- *node server.js*

**Launch HTML Page:**

In Firefox, Chrome, Opera...

- *https://localhost:8080*

***!! Launch the Database before the Server !!***